import Link from "next/link";
import React from "react";

function NotfoundView() {
  return (
    <div className="font-Titre  h-screen bg-black flex flex-col justify-center items-center">
      <p className=" text-white text-6xl ">Page introuvable</p>
      <p className="text-[300px] text-white">404</p>
      <a rel="stylesheet" href={"/"}>
        <button className="bg-white rounded-3xl text-2xl s w-24 h-12 font-semibold">
          Retour
        </button>
      </a>
    </div>
  );
}

export default NotfoundView;
