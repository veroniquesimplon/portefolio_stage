import React from "react";
import Layout from "../Layout/Layout";
import Presentation from "../Components/Presentation";
import Text from "../Components/Text";
import { Project, Technology } from "@/typings";
import Projects from "../Components/Projects";
import Form from "../Components/Form";
import Scroll from "../Components/Scroll";

interface Props {
  projects: Project[];
  technologies: Technology[];
}

function PortfolioView({ projects, technologies }: Props) {
  return (
    
     <Layout>
      <Presentation/>
      <Text/>
      <Projects
      projects={projects}
      technologies={technologies} />
      <Form/>
     </Layout>
  );
}

export default PortfolioView;
