import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

function Form() {
  const [message, setmessage] = useState(false);

  const [submitted, setSubmitted] = useState(false);
  const [success, setSuccess] = useState(false);

  const validation = yup.object().shape({
    firstName: yup.string().required("Veuillez saisir votre prénom"),
    lastName: yup.string().required("Veuillez saisir votre nom"),
    email: yup.string().email().required("Veuillez saisir votre email"),

    message: yup.string().required("Veuillez saisir votre message"),
  });
  const optionsForm = { resolver: yupResolver(validation) };
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(optionsForm);

  const submitForm = async (data: any) => {
    const { firstName, lastName, email, message } = data;
    console.log(data);
    setSubmitted(true);

    await fetch("/api/createForm", {
      method: "POST",
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        message,
      }),
    })
      .then((res) => {
        if (res.status === 200) {
          setSuccess(true);
        } else {
          setSuccess(false);
          // console.log("Erreur !!!");
        }
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setSubmitted(false);
      });
  };

  return (
    <div id="sectionForm" className="bg-black text-white p-10">
      <h3 className="text-3xl flex justify-center font-Titre">Contactez-moi</h3>
      {success ? (
        <div className=" flex flex-col items-center justify-center gap-y-3 text-white font-Texte pt-16">
        <p className=" text-2xl font-semibold ">Message envoyé !</p>
        <p>Vous serez contacter sous peu !</p></div>
      ) : (
        <form
          onSubmit={handleSubmit(submitForm)}
          className={`font-Text text-xl ${submitted && "pointer-events-none animate-pulse"}`}
        >
          <div className="flex">
            <div className="w-1/2 pt-8 flex justify-end border border-pink-500">
              <div className="w-1/2 flex flex-col items-center  justify-between  border border-yellow-500">
                <div className="flex gap-x-10  border border-green-500">
                <label htmlFor="lastName" className=" w-1/2">
                    Nom
                  </label>
                  <input
                    {...register("lastName")}
                    id="lastName"
                    type="text"
                    className="text-black"
                  />
                </div>
                <div className="text-base text-red-500">
                  {errors.lastName && <p>{errors.lastName.message}</p>}
                </div>

                <div className="flex gap-x-5">
                  <label htmlFor="firstName" className=" w-1/2">
                    Prénom
                  </label>
                  <input
                    {...register("firstName")}
                    id="firstName"
                    type="text"
                    className="text-black"
                  />
                </div>
                <div className="text-base text-red-500">
                  {errors.firstName && <p>{errors.firstName.message}</p>}
                </div>
              </div>
            </div>

            <div className="w-1/2 pt-8 flex justify-start  border border-pink-500">
              <div className="w-1/2 flex flex-col items-center gap-y-8 justify-between">
                <div className="flex gap-x-6">
                  <label htmlFor="phoneNumber" className="w-1/2">
                    N° Téléphone
                  </label>
                  <input
                    id="phoneNumber"
                    type="text"
                    className="text-black"
                  />
                </div>
                <div className="flex gap-x-5">
                  <label htmlFor="email" className=" w-1/2">
                    Adresse Email
                  </label>
                  <input
                    {...register("email")}
                    id="email"
                    type="text"
                    className="text-black"
                  />
                </div>
                <div className="pl-16 text-base text-red-500">
                  {errors.email && <p>{errors.email.message}</p>}
                </div>
              </div>
            </div>
          </div>
          {/*commentaire */}
          <div className="flex flex-col items-center gap-y-8 pt-8">
            <div className="flex gap-x-5">
              <label htmlFor="commentaire">Commentaire</label>
              <input
                {...register("message")}
                id="commentaire"
                type="text"
                className="h-52 w-[400px] text-black"
              />
            </div>
            <div className="text-base text-red-500">
              {errors.message && <p>{errors.message.message}</p>}
            </div>

            {/*boutton envoyer */}
            <div className="flex justify-end w-[540px] ">
              <div className="text-black bg-white rounded-xl w-24 flex justify-center">
                <button
                  // className={`${message ? `bg-yellow-500` : `bg-green-400`}`}
                  // onClick={() => setmessage(true)}
                  type="submit"
                  value="Envoyer"
                >
                  Envoyer
                </button>
              </div>
            </div>
            {message && (
              <div className="h-screen relative top-4">
                <div className="absolute">
                  <p className="bg-white text-black w-72 h-24 rounded-xl flex items-center justify-center font-Texte text-2xl">
                    Message envoyé
                  </p>
                </div>
              </div>
            )}
          </div>
        </form>
      )}
    </div>
  );
}

export default Form;
