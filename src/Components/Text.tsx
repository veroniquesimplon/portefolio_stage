import Link from "next/link";
import React from "react";

function Text() {
  return (
    <div className=" text-xl font-Text bg-black w-2/3">
      <div className="bg-black text-[#f5f5f5] w-4/5 left-0 pl-8">
        <p>
          {`Bienvenue sur mon portfolio ! Diplômée d'un DUT Techniques de
          Commercialisation, j'ai entrepris l'année dernière une reconversion
          passionnante dans le domaine de l'informatique. Cette transition a
          débuté avec une formation de découverte qui a suscité en moi un vif
          intérêt pour le développement web. Encouragée par cette expérience
          enrichissante, j'ai eu l'opportunité cette année de poursuivre mon
          apprentissage avec Simplon, la formation de développeur web et web
          mobile. 
          <br />
          Dans ce portfolio, vous trouverez une sélection de mes
          premiers projets, témoignant de mon parcours d'apprentissage et de mes
          compétences acquises en développement web. J'espère que vous
          apprécierez parcourir mes réalisations autant que j'ai apprécié les
          créer. `}
          
      
        </p>
      </div>
    </div>
  );
}

export default Text;
