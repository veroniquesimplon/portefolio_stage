import { urlFor } from "@/sanity";
import { Project, Technology } from "@/typings";
import Image from "next/image";
import Link from "next/link";
import React from "react";

interface Props {
  project: Project
  technologies: Technology[];
}
function ProjectListItem({project, technologies}:Props) {
  console.log(technologies)
  return (
    <div className="w-80 flex flex-shrink-0 flex-col justify-end bg-black gap-3">
      <div className="relative w-full h-80">
        <Image
          className="object-cover"
          fill
          src={urlFor(project?.photo).url()!}
          alt="project's image"
        />
      </div>
      <Link className="font-Titre text-xl" href={`/projects/${project?.slug?.current}`}>{project?.title}</Link>

      <div className="flex flex-row"> 
      {project?.technologies?.map((technology) => (
          <div
            key={technology?._id}
            className="h-11 w-11 aspect-square relative"
          >
            <Image 
            src={urlFor(technology?.logo).url()!} 
            alt="icon"
            fill
            className=" asbolute object-cover "
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default ProjectListItem;
