import React, { useEffect, useState } from "react";
import { ChatBubbleLeftIcon, PhoneIcon } from "@heroicons/react/24/solid";
import { EnvelopeIcon } from "@heroicons/react/24/solid";


function Presentation() {
  const [gradient, setGradient] = useState(
    "linear-gradient(to bottom, white, black)"
  );

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      const maxScroll =
        document.documentElement.scrollHeight - window.innerHeight;
      const scrollFraction = Math.min(scrollPosition / maxScroll, 1);
      // scrollFraction = Math.pow(scrollFraction, 2);

      const startColor = "#f5f5f5"; // White
      const endColor = `#000000`; // Black with varying opacity rgba(0, 0, 0, ${scrollFraction})
      setGradient(`linear-gradient(to bottom, ${startColor}, ${endColor})`);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div className="flex w-full h-[780px]">
      <div className=" bg-black w-2/3 text-[#F5F5F5] h-[780px]">
        {/*relative min-h-screen*/}
        <div className="flex flex-col items-end justify-center mx-auto w-11/12 gap-10 h-full">
          {/*h-screen */}
          <div className="space-y-6 font-Titre">
            <h1 className="text-7xl ">RAMRADJ</h1>
            <h1 className="text-7xl pl-10">{`Véronique`}</h1>
          </div>
          <div>
            <h3 className="text-4xl font-Text">Développeur web</h3>
          </div>
        </div>
      </div>
      <div className=" w-1/3 h-[780px] flex justify-center items-center">
        <div className="  text-black font-Text text-lg flex flex-col gap-y-8">
          <div className="flex items-center gap-4">
          <a href="#sectionForm"> <ChatBubbleLeftIcon className="h-10" /></a>
          <a href="#sectionForm"><p>Formulaire de contact</p></a>
          </div>
          <div className="flex items-center gap-4">
            <EnvelopeIcon className="h-10" />
            <p>veroniqueramradj@gmail.com</p>
          </div>
          <div className="flex items-center gap-5">
        <PhoneIcon className="h-9 w-11" />
            <p>07 68 01 74 46</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Presentation;
