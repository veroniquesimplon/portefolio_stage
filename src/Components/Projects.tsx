import React, { useEffect, useRef, useState } from "react";
import ProjectListItem from "./ProjectListItem";
import { Project, Technology } from "@/typings";

 
interface Props {
  projects: Project[];
  technologies: Technology[];
}

function Projects({ projects, technologies }: Props) {
  const [scrollPosition, setScrollPosition] = useState({ scrollTop: 0, scrollLeft: 0 });
  const scrollDemoRef = useRef(null);

  const handleScroll = () => {
    if (scrollDemoRef.current) {
      const { scrollTop, scrollLeft } = scrollDemoRef.current;
      setScrollPosition({ scrollTop, scrollLeft });
    }
  };

  return (
    <div className="bg-black h-[800px] flex items-center overflow-hidden">
      <div 
        className="h-[550px] bg-black text-[#f5f5f5] pl-8 flex gap-8 overflow-x-auto scrollbar-hide"
        // onScroll={handleScroll}
        // ref={scrollDemoRef}
      >
        {projects.map((project, index) => (
          <div
            key={project?._id}
            className={`snap-start ${index % 2 === 0 ? "mt-0" : "mt-20"}`}
          >
            <ProjectListItem 
            project={project} 
            technologies={technologies}/>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Projects;