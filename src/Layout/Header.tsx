import Image from "next/image";
import React from "react";

function Header() {
  return (
    <div className="pl-8 bg-black  w-auto h-16 flex items-center border-b border-[#f5f5f5]">
      <a rel="stylesheet" href={"/"}>
        <div className="flex">
          <p className="font-Titre font-semibold text-white text-[50px]">V</p>
          <p className="font-Titre italic text-white text-3xl pt-6">R</p>
        </div>
      </a>
    </div>
  );
}

export default Header;
