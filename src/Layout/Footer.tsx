import React from "react";

function Footer() {
  return (
    <div className="font-Text h-12 bg-black flex justify-end items-center border-t border-[#f5f5f5] ">
      <p>&copy;2024 Tous droits réservés.</p>
    </div>
  );
}

export default Footer;
