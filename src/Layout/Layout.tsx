import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import { useRouter } from "next/router";
import Head from "next/head";

function Layout({
  children,
  title = "portfolio",
  description = "mon portfolio",
  keywords = "portfolio",
}: any) {
  const router = useRouter();
  return (
    <div className="flex flex-col bg-white text-white flex-1 min-h-screen scroll-smooth w-full">
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="keywords" content={keywords} />
        <meta name="description" content={description} />
        <meta charSet="utf-8" />
        <title>{title}</title>
        <link rel="icon" href="" />
      </Head>

      <Header />
      <main className="flex flex-col flex-1 w-full min-h-screen">{children}</main>
      <Footer />
    </div>
  );
}

export default Layout;
