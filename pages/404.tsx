import NotfoundView from "@/src/Views/NotfoundView";
import React from "react";

function NotFound() {
  return <NotfoundView/>;
}

export default NotFound;