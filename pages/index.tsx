import { Project, Technology } from "@/typings";
import { sanityClient } from "../sanity";
import PortfolioView from "@/src/Views/PortfolioView";

interface Props {
  projects: Project[];
  technologies: Technology[];
}

export default function Home({ projects, technologies }: Props) {
  console.log(projects);
  return (
    <>
      <PortfolioView projects={projects} technologies={technologies} />
    </>
  );
}

export const getServerSideProps = async () => {
  const projectQuery = `*[_type == "project"]{
_id,
title,
description,
photo,
photo1, 
photo2,
visibility,
_createdAt,
technologies[]->{
      _id,
      title,
      logo,
      _createdAt,
    }, 
link,
slug{
current
}}`;

  const projects = await sanityClient.fetch(projectQuery);

  const technologyQuery = `*[_type == "technology"]{
  _id,
  title,
  logo,
  _createdAt,
}`;
  const technologies = await sanityClient.fetch(technologyQuery);

  return {
    props: {
      projects,
      technologies,
    },
  };
};
