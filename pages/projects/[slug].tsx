import { sanityClient, urlFor } from "@/sanity";
import Layout from "@/src/Layout/Layout";
import { Project } from "@/typings";
import { GetStaticProps } from "next";
import Image from "next/image";
import React from "react";

interface Props {
  project: Project;
}

function OneProject({ project }: Props) {
  return (
    <Layout>
    <div className="bg-black h-screen">
      <div className=" ">
        <h1 className="font-Titre text-4xl text-white font-semibold flex justify-center p-5 pt-16">
          {project?.title}
        </h1>
        <p className=" text-white flex justify-center text-lg font-Text ">
          {project?.description}
        </p>
      </div>
      <div className="relative  lg:flex justify-center gap-x-6 ">
        <div className="relative h-[700px] w-[800px] lg:h-[500px] lg:w-[1000px] top-16">
          <Image
            className="object-contain lg:object-cover"
            fill
            src={urlFor(project?.photo1).url()!}
            alt="skin care project picture"
          />
        </div>
        <div className="relative top-16 h-[700px] w-[800px]">
          <Image
            className="object-contain"
            fill
            src={urlFor(project?.photo2).url()!}
            alt="skin care project picture"
          />
        </div>
      </div>
      <p className=" text-xl font-Text absolute left-12 bottom-24  text-white font-medium ">
        Le lien du projet
      </p>
    </div>
    </Layout>
  );
}

export default OneProject;

export async function getStaticPaths() {
  const query = `*[_type == "project"]{
        _id,
        slug {
            current
        }
    } `;

  const projects = await sanityClient.fetch(query);

  const paths = projects
    .map((project: any) => ({
      params: {
        slug: project?.slug?.current,
      },
    }))

    .flat();

  return {
    paths,
    fallback: "blocking",
  };
}

export const getStaticProps: GetStaticProps = async ({ params }: any) => {
  const projectQuery = `*[_type == "project" &&  slug.current == $slug ][0] {
    _id,
    title,
    description,
    photo,
    photo1, 
    photo2,
    visibility,
    _createdAt,
    link,
    slug{
    current
    }
    }`;

  const project = await sanityClient.fetch(projectQuery, {
    slug: params?.slug,
  });

  if (!project) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      project,
    },
    revalidate: 10,
  };
};
