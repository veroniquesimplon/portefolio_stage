export interface Project {
  _id: string;
  _createdAt: string;
  title: string;
  description: string;
  photo: {
    asset: {
      url: string;
    };
  };
  photo1: {
    asset: {
      url: string;
    };
  };
  photo2: {
    asset: {
      url: string;
    };
  };

  visibility: boolean;
  link: string;
  slug: {
    current: string;
  };
  technologies: Technology[];
}

export interface Technology {
  _id: string;
  _createdAt: string;
  title: string;
  logo: {
    asset: {
      url: string;
    };
  };

}
